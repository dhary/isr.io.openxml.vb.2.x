﻿''' <summary>
''' Manages conversions to english meteric units.
''' See http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
''' http://www.oreillynet.com/xml/blog/2007/04/what_is_an_emu.html
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/23/2011" by="David Hary" revision="2.0.4373.x">
''' Created
''' </history>
Public Class EnglishMetricUnits

    Private Const _unitsPerMilimeter As Long = 36000
    ''' <summary>
    ''' Gets the units per milimeter.
    ''' </summary>
    ''' 
    Public Shared ReadOnly Property UnitsPerMillimeter As Long
        Get
            Return _unitsPerMilimeter
        End Get
    End Property

    Private Const _unitsPerInch As Long = 914400
    ''' <summary>
    ''' Gets the units per inch.
    ''' </summary>
    ''' 
    Public Shared ReadOnly Property UnitsPerInch As Long
        Get
            Return _unitsPerInch
        End Get
    End Property

    ''' <summary>
    ''' Converts from pixels.
    ''' Uses default 72 DPI. 
    ''' </summary>
    ''' <param name="value">The value.</param><returns></returns>
    Public Shared Function ConvertFromPixels(ByVal value As Integer) As Long
        Return EnglishMetricUnits.ConvertFromPixels(value, 72)
    End Function

    ''' <summary>
    ''' Converts from pixels.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="pixelsPerInch">The pixels per inch.</param><returns></returns>
    Public Shared Function ConvertFromPixels(ByVal value As Integer, ByVal pixelsPerInch As Integer) As Long
        Return EnglishMetricUnits.ConvertFromInches(value / pixelsPerInch)
    End Function

    ''' <summary>
    ''' Converts from milimeters.
    ''' </summary>
    ''' <param name="value">The value.</param><returns></returns>
    Public Shared Function ConvertFromMillimeters(ByVal value As Double) As Long
        Return CLng(value * EnglishMetricUnits.UnitsPerMillimeter)
    End Function

    ''' <summary>
    ''' Converts from inches.
    ''' </summary>
    ''' <param name="value">The value.</param><returns></returns>
    Public Shared Function ConvertFromInches(ByVal value As Double) As Long
        Return CLng(value * EnglishMetricUnits.UnitsPerInch)
    End Function

    ''' <summary>
    ''' Converts from milimeters.
    ''' </summary>
    ''' <param name="value">The value.</param><returns></returns>
    Public Shared Function ConvertFromMillimeters(ByVal value As Single) As Long
        Return CLng(value * EnglishMetricUnits.UnitsPerMillimeter)
    End Function

    ''' <summary>
    ''' Converts from inches.
    ''' </summary>
    ''' <param name="value">The value.</param><returns></returns>
    Public Shared Function ConvertFromInches(ByVal value As Single) As Long
        Return CLng(value * EnglishMetricUnits.UnitsPerInch)
    End Function

End Class
