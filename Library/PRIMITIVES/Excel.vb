﻿Imports System.Drawing
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports DocumentFormat.OpenXml.Drawing.Spreadsheet
Imports System.Text.RegularExpressions

''' <summary>
''' Manages an Excel open XML document.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' Includes, open Source contributions from:
''' http://polymathprogrammer.com/store/spreadsheet-open-xml/
''' and
''' http://msdn.microsoft.com/en-us/library/cc880096.aspx
''' and
''' http://lateral8.com/articles/2010/6/11/openxml-sdk-20-formatting-excel-values.aspx
''' as licensed therein.
''' </license>
''' <history date="07/12/2011" by="David Hary" revision="2.0.4210.x">
''' Created
''' </history>
Public Class Excel
    Implements IDisposable

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Excel" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    ''' class should not be able to override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by its disposing parameter.
    ''' If True, the method has been called directly or indirectly by a user's code--managed 
    ''' and unmanaged resources can be disposed. If disposing equals False, the method has been 
    ''' called by the runtime from inside the finalizer and you should not reference other 
    ''' objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                ' Free shared managed resources
                If disposing Then

                    _sheet = Nothing
                    _workBookPart = Nothing
                    _workSheetPart = Nothing
                    _workSheet = Nothing

                    If _document IsNot Nothing Then
                        _document.Dispose()
                        _document = Nothing
                    End If

                End If

        ' Free shared unmanaged resources

            Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method does not get called. 
    ''' It gives the base class the opportunity to finalize. Do not provide destructors 
    ''' in types derived from this class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here. Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()
    End Sub

#End Region

#Region " SHARED TABLES "

    ''' <summary>
    ''' Inserts the shared string item.
    ''' </summary>
    ''' <param name="workbookPart">The workbook part.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given the main workbook part, and a text value, insert the text into the shared string table. 
    ''' Create the table if necessary. If the value already exists, return its index. If it doesn't exist, 
    ''' insert it and return its new index.
    ''' </remarks>
    Public Shared Function InsertSharedStringItem(ByVal workbookPart As WorkbookPart, ByVal value As String) As Integer

        ' Insert a value into the shared string table, creating the table if necessary.
        ' Insert the string if it's not already there.
        ' Return the index of the string.

        Dim index As Integer = 0
        Dim found As Boolean = False
        Dim stringTablePart = workbookPart.GetPartsOfType(Of SharedStringTablePart).FirstOrDefault()

        ' If the shared string table is missing, something's wrong.
        ' Just return the index that you found in the cell.
        ' Otherwise, look up the correct text in the table.
        If stringTablePart Is Nothing Then
            ' Create it.
            stringTablePart = workbookPart.AddNewPart(Of SharedStringTablePart)()
        End If

        Dim stringTable = stringTablePart.SharedStringTable
        If stringTable Is Nothing Then
            stringTable = New SharedStringTable
        End If

        ' Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
        For Each item As SharedStringItem In stringTable.Elements(Of SharedStringItem)()
            If (item.InnerText = value) Then
                found = True
                Exit For
            End If
            index += 1
        Next

        If Not found Then
            stringTable.AppendChild(New SharedStringItem(New DocumentFormat.OpenXml.Spreadsheet.Text(value)))
            stringTable.Save()
        End If

        Return index
    End Function

#End Region

#Region " CELL - SHARED "

    ''' <summary>
    ''' Gets the cell for reading.
    ''' </summary>
    ''' <param name="document">The document.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="address">The cell address.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a reference to an Excel SpreadsheetDocument, the name of a sheet, and a cell address, 
    ''' return a reference to the cell. 
    ''' Throw an ArgumentException if the sheet doesn't exist, or if the cell doesn't yet exist.
    ''' </remarks>
    Public Shared Function GetCellForReading(ByVal document As SpreadsheetDocument, ByVal sheetName As String, ByVal address As String) As Cell

        Dim wbPart As WorkbookPart = document.WorkbookPart

        ' Find the sheet with the supplied name, and then use that Sheet object
        ' to retrieve a reference to the appropriate worksheet.
        Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)().Where(Function(s) s.Name = sheetName).FirstOrDefault()

        If theSheet Is Nothing Then
            Throw New ArgumentException("sheetName")
        End If

        ' Retrieve a reference to the worksheet part, and then use its Worksheet property to get 
        ' a reference to the cell whose address matches the address you've supplied:
        Dim wsPart As WorksheetPart = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart)
        Dim theCell As Cell = wsPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = address).FirstOrDefault

        ' If the cell doesn't exist, raise an exception to the caller:
        If theCell Is Nothing Then
            Throw New ArgumentException("address")
        End If
        Return theCell

    End Function

    ''' <summary>
    ''' Gets the cell for writing.
    ''' </summary>
    ''' <param name="document">The document.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="address">The cell address.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a reference to an Excel SpreadsheetDocument, the name of a sheet, and a cell address, 
    ''' return a reference to the cell. Throw an ArgumentException if the sheet doesn't exist. 
    ''' If the cell doesn't exist, create it.
    ''' </remarks>
    Public Shared Function GetCellForWriting(ByVal document As SpreadsheetDocument, ByVal sheetName As String, ByVal address As String) As Cell

        Dim wbPart As WorkbookPart = document.WorkbookPart

        ' Find the sheet with the supplied name, and then use that Sheet object
        ' to retrieve a reference to the appropriate worksheet.
        Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)().Where(Function(s) s.Name = sheetName).FirstOrDefault()

        If theSheet Is Nothing Then
            Throw New ArgumentException("sheetName")
        End If

        ' Retrieve a reference to the worksheet part, and then use its Worksheet property to get 
        ' a reference to the cell whose address matches the address you've supplied:
        Dim wsPart As WorksheetPart = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart)
        Dim ws As Worksheet = wsPart.Worksheet
        Dim theCell As Cell = ws.Descendants(Of Cell).Where(Function(c) c.CellReference = address).FirstOrDefault

        ' If the cell doesn't exist, create it:
        If theCell Is Nothing Then
            theCell = InsertCell(ws, address)
        End If
        Return theCell
    End Function

    ''' <summary>
    ''' Gets the cell value.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="addressName">The cell address.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetCellValue(ByVal fileName As String, ByVal sheetName As String, ByVal addressName As String) As String

        Dim value As String = Nothing

        Using document As SpreadsheetDocument = SpreadsheetDocument.Open(fileName, False)

            Dim wbPart As WorkbookPart = document.WorkbookPart

            ' Find the sheet with the supplied name, and then use that Sheet object
            ' to retrieve a reference to the appropriate worksheet.
            Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)().Where(Function(s) s.Name = sheetName).FirstOrDefault()

            If theSheet Is Nothing Then
                Throw New ArgumentException("sheetName")
            End If

            ' Retrieve a reference to the worksheet part, and then use its Worksheet property to get 
            ' a reference to the cell whose address matches the address you've supplied:
            Dim wsPart As WorksheetPart = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart)
            Dim theCell As Cell = wsPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = addressName).FirstOrDefault

            ' If the cell doesn't exist, raise an exception to the caller:
            If theCell Is Nothing Then
                Throw New ArgumentException("address")
            End If
            value = theCell.InnerText

            ' If the cell represents an integer number, you're done. 
            ' For dates, this code returns the serialized value that 
            ' represents the date. The code handles strings and booleans
            ' individually. For shared strings, the code looks up the corresponding
            ' value in the shared string table. For booleans, the code converts 
            ' the value into the words TRUE or FALSE.
            If theCell.DataType IsNot Nothing Then
                Select Case theCell.DataType.Value
                    Case CellValues.SharedString
                        ' For shared strings, look up the value in the shared strings table.
                        Dim stringTable = wbPart.GetPartsOfType(Of SharedStringTablePart).FirstOrDefault()
                        ' If the shared string table is missing, something's wrong.
                        ' Just return the index that you found in the cell.
                        ' Otherwise, look up the correct text in the table.
                        If stringTable IsNot Nothing Then
                            value = stringTable.SharedStringTable.ElementAt(Integer.Parse(value)).InnerText
                        End If
                    Case CellValues.Boolean
                        Select Case value
                            Case "0"
                                value = "FALSE"
                            Case Else
                                value = "TRUE"
                        End Select
                End Select
            End If
        End Using
        Return value
    End Function

    ''' <summary>
    ''' Gets the cell value.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="colNumber">The col number.</param>
    ''' <param name="row">The row.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a workbook file, a sheet name, and row and column integer values, retrieve the value of the cell. 
    ''' Call the function like this to retrieve the value from cell B3:
    ''' <code>
    ''' Dim value As String = XLGetCellValueRowCol("Sample.xlsx", "Sheet1", 2, 3)
    ''' </code> 
    ''' </remarks>
    Public Shared Function GetCellValue(ByVal fileName As String, ByVal sheetName As String, ByVal colNumber As Integer, ByVal row As Integer) As String
        ' The challenge: Convert a cell number into a letter name.
        Return GetCellValue(fileName, sheetName, GetColumnName(colNumber), row)
    End Function

    ''' <summary>
    ''' Gets the cell value.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="colName">Name of the col.</param>
    ''' <param name="row">The row.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a workbook file, a sheet name, and row and column names, retrieve the value of the cell. 
    ''' Call the function like this: 
    ''' <code>
    ''' Dim value As String = XLGetCellValueRowCol("Sample.xlsx", "Sheet1", "B", 3)
    ''' </code>
    ''' </remarks>
    Public Shared Function GetCellValue(ByVal fileName As String, ByVal sheetName As String, ByVal colName As String, ByVal row As Integer) As String

        Dim value As String = Nothing

        Using document As SpreadsheetDocument = SpreadsheetDocument.Open(fileName, False)

            Dim wbPart As WorkbookPart = document.WorkbookPart

            ' Find the sheet with the supplied name, and then use that Sheet object
            ' to retrieve a reference to the appropriate worksheet.
            Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)(). _
              Where(Function(s) s.Name = sheetName).FirstOrDefault()

            If theSheet Is Nothing Then
                Throw New ArgumentException("sheetName")
            End If

            ' Retrieve a reference to the worksheet part, and then use its Worksheet property to get 
            ' a reference to the cell whose address matches the address you've supplied:
            Dim wsPart As WorksheetPart = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart)
            Dim theCell As Cell = wsPart.Worksheet.Descendants(Of Cell). _
              Where(Function(c) c.CellReference = colName & row.ToString()).FirstOrDefault

            ' If the cell doesn't exist, return an empty string.
            If theCell IsNot Nothing Then
                value = theCell.InnerText

                ' If the cell represents an integer number, you're done. 
                ' For dates, this code returns the serialized value that 
                ' represents the date. The code handles strings and booleans
                ' individually. For shared strings, the code looks up the corresponding
                ' value in the shared string table. For booleans, the code converts 
                ' the value into the words TRUE or FALSE.
                If theCell.DataType IsNot Nothing Then
                    Select Case theCell.DataType.Value
                        Case CellValues.SharedString
                            ' For shared strings, look up the value in the shared strings table.
                            Dim stringTable = wbPart.GetPartsOfType(Of SharedStringTablePart).FirstOrDefault()
                            ' If the shared string table is missing, something's wrong.
                            ' Just return the index that you found in the cell.
                            ' Otherwise, look up the correct text in the table.
                            If stringTable IsNot Nothing Then
                                value = stringTable.SharedStringTable.ElementAt(Integer.Parse(value)).InnerText
                            End If
                        Case CellValues.Boolean
                            Select Case value
                                Case "0"
                                    value = "FALSE"
                                Case Else
                                    value = "TRUE"
                            End Select
                    End Select
                End If
            End If
        End Using
        Return value
    End Function

    ''' <summary>
    ''' Inserts the cell in worksheet.
    ''' </summary>
    ''' <param name="workSheet">The work sheet.</param>
    ''' <param name="addressName">Name of the address.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a Worksheet and an address (like "AZ254"), either return a cell reference, 
    ''' or create the cell reference and return it.
    ''' </remarks>
    Public Shared Function InsertCell(ByVal workSheet As Worksheet, _
                                           ByVal addressName As String) As Cell

        ' Use regular expressions to get the row number and column name.
        ' If the parameter wasn't well formed, this code
        ' will fail:
        Dim rx As New Regex("^(?<col>\D+)(?<row>\d+)")
        Dim m As Match = rx.Match(addressName)
        Dim rowNumber As UInteger = UInteger.Parse(m.Result("${row}"))
        Dim colName As String = m.Result("${col}")

        Dim sheetData As SheetData = workSheet.GetFirstChild(Of SheetData)()
        Dim cellReference As String = (colName & rowNumber.ToString())
        Dim theCell As Cell = Nothing

        ' If the worksheet does not contain a row with the specified row index, insert one.
        Dim theRow = sheetData.Elements(Of Row).Where(Function(r) r.RowIndex.Value = rowNumber).FirstOrDefault()
        If theRow Is Nothing Then
            theRow = New Row()
            theRow.RowIndex = rowNumber
            sheetData.Append(theRow)
        End If

        ' If the cell you need already exists, return it.
        ' If there is not a cell with the specified column name, insert one.  
        Dim refCell As Cell = theRow.Elements(Of Cell). _
          Where(Function(c) c.CellReference.Value = cellReference).FirstOrDefault()
        If refCell IsNot Nothing Then
            theCell = refCell
        Else
            ' Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
            For Each cell As Cell In theRow.Elements(Of Cell)()
                If (String.Compare(cell.CellReference.Value, cellReference, True) > 0) Then
                    refCell = cell
                    Exit For
                End If
            Next

            theCell = New Cell
            theCell.CellReference = cellReference

            theRow.InsertBefore(theCell, refCell)
        End If
        Return theCell
    End Function

    ''' <summary>
    ''' Inserts the number into cell.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="addressName">Cell address, e.g., 'C3'</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a file, a sheet, and a cell, insert a specified value. 
    ''' For example: InsertNumberIntoCell("C:\Test.xlsx", "Sheet3", "C3", 14)
    ''' </remarks>
    Public Shared Function InsertCellValue(ByVal fileName As String, ByVal sheetName As String,
                                         ByVal addressName As String, ByVal value As Integer) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        ' Open the document for editing.
        Using document As SpreadsheetDocument = SpreadsheetDocument.Open(fileName, True)
            Dim wbPart As WorkbookPart = document.WorkbookPart

            Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)(). _
              Where(Function(s) s.Name = sheetName).FirstOrDefault()
            If theSheet IsNot Nothing Then
                Dim ws As Worksheet = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart).Worksheet
                returnValue = InsertCellValue(ws, addressName, value)
                If returnValue Then
                    ' Save the worksheet.
                    ws.Save()
                End If
                returnValue = True
            End If
        End Using

        Return returnValue
    End Function

    ''' <summary>
    ''' Inserts the number into cell.
    ''' </summary>
    ''' <param name="workSheet">The work sheet.</param>
    ''' <param name="addressName">Cell address, e.g., 'C3'</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertCellValue(ByVal workSheet As Worksheet, ByVal addressName As String, ByVal value As Integer) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        Dim theCell As Cell = InsertCell(workSheet, addressName)
        returnValue = SetCellValue(theCell, value)

        Return returnValue

    End Function

    ''' <summary>
    ''' Inserts the string into cell.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param>
    ''' <param name="addressName">Cell address, e.g., 'C3'</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a file, a sheet, and a cell, insert a specified string. 
    ''' For example: InsertStringIntoCell("C:\Test.xlsx", "Sheet3", "C3", "Microsoft")
    ''' If the string exists in the shared string table, get its index. 
    ''' If the string doesn't exist in the shared string table, add it and get the next index. 
    ''' Then, the remainder is the same as inserting a number, but insert the string index 
    ''' instead of a value. Also, set the cell's t attribute to be the value "s".
    ''' </remarks>
    Public Shared Function InsertCallValue(ByVal fileName As String, ByVal sheetName As String,
                                    ByVal addressName As String, ByVal value As String) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        ' Open the document for editing.
        Using document As SpreadsheetDocument = SpreadsheetDocument.Open(fileName, True)
            Dim wbPart As WorkbookPart = document.WorkbookPart

            Dim theSheet As Sheet = wbPart.Workbook.Descendants(Of Sheet)(). _
              Where(Function(s) s.Name = sheetName).FirstOrDefault()

            If theSheet IsNot Nothing Then
                Dim ws As Worksheet = CType(wbPart.GetPartById(theSheet.Id), WorksheetPart).Worksheet
                Dim theCell As Cell = InsertCell(ws, addressName)

                ' Either retrieve the index of an existing string,
                ' or insert the string into the shared string table
                ' and get the index of the new item.
                Dim stringIndex As Integer = InsertSharedStringItem(wbPart, value)

                theCell.CellValue = New CellValue(stringIndex.ToString())
                theCell.DataType = New EnumValue(Of CellValues)(CellValues.SharedString)

                ' Save the worksheet.
                ws.Save()
                returnValue = True
            End If
        End Using

        Return returnValue
    End Function

#End Region

#Region " CELL - SET CELL VALUE - WORK SHEET CELL "

    ''' <summary>
    ''' Sets the cell style index.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplyCellStyleBackground(ByVal workSheetCell As Cell, ByVal value As Integer) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.StyleIndex = CType(value, UInt32Value)
            returnValue = True
        End If

        Return returnValue

    End Function


    ''' <summary>
    ''' Sets the cell style index.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ApplyCellStyleIndex(ByVal workSheetCell As Cell, ByVal value As Integer) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.StyleIndex = CType(value, UInt32Value)
            returnValue = True
        End If

        Return returnValue

    End Function

    ''' <summary>
    ''' Sets the cell value to the number.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetCellValue(ByVal workSheetCell As Cell, ByVal value As Integer) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then

            workSheetCell.CellValue = New CellValue(value.ToString())
            workSheetCell.DataType = New EnumValue(Of CellValues)(CellValues.Number)

            returnValue = True
        End If

        Return returnValue

    End Function

    ''' <summary>
    ''' Sets the cell value to the number.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetCellValue(ByVal workSheetCell As Cell, ByVal value As Single) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.CellValue = New CellValue(value.ToString())
            workSheetCell.DataType = New EnumValue(Of CellValues)(CellValues.Number)
            returnValue = True
        End If

        Return returnValue

    End Function

    ''' <summary>
    ''' Sets the cell value to the number.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetCellValue(ByVal workSheetCell As Cell, ByVal value As Double) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.CellValue = New CellValue(value.ToString())
            workSheetCell.DataType = New EnumValue(Of CellValues)(CellValues.Number)
            returnValue = True
        End If

        Return returnValue

    End Function

    ''' <summary>
    ''' Sets the cell value to the date.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetCellValue(ByVal workSheetCell As Cell, ByVal value As DateTime) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.CellValue = New CellValue(value.ToOADate().ToString())
            '            workSheetCell.DataType = New EnumValue(Of CellValues)(CellValues.Date)
            returnValue = True
        End If

        Return returnValue

    End Function

    ''' <summary>
    ''' Sets the cell value to the string.
    ''' </summary>
    ''' <param name="workSheetCell">The work sheet cell.</param>
    ''' <param name="value">The value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetCellValue(ByVal workSheetCell As Cell, ByVal value As String) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If workSheetCell IsNot Nothing Then
            workSheetCell.CellValue = New CellValue(value)
            workSheetCell.DataType = New EnumValue(Of CellValues)(CellValues.String)
            returnValue = True
        End If

        Return returnValue

    End Function

#End Region

#Region " CELL - GET CELL.. "

    ''' <summary>
    ''' Gets the cell.
    ''' </summary>
    ''' <param name="colName">Name of the col.</param>
    ''' <param name="row">The row.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCell(ByVal colName As String, ByVal row As Integer) As Cell

        Return _workSheetPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = colName & row.ToString()).FirstOrDefault

    End Function

    ''' <summary>
    ''' Gets the cell.
    ''' </summary>
    ''' <param name="address">The cell address.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCell(ByVal address As String) As Cell

        Return _workSheetPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = address).FirstOrDefault

    End Function

    ''' <summary>
    ''' Gets the cell for reading.
    ''' </summary>
    ''' <param name="address">The cell address.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Given a reference to an Excel SpreadsheetDocument, the name of a sheet, and a cell address, 
    ''' return a reference to the cell. 
    ''' Throw an ArgumentException if the sheet doesn't exist, or if the cell doesn't yet exist.
    ''' </remarks>
    Public Function GetCellForReading(ByVal address As String) As Cell

        Dim theCell As Cell = _workSheetPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = address).FirstOrDefault

        ' If the cell doesn't exist, raise an exception to the caller:
        If theCell Is Nothing Then
            Throw New ArgumentException("address")
        End If
        Return theCell

    End Function

    ''' <summary>
    ''' Gets the cell for writing.
    ''' </summary>
    ''' <param name="address">The address.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCellForWriting(ByVal address As String) As Cell

        Dim theCell As Cell = _workSheetPart.Worksheet.Descendants(Of Cell).Where(Function(c) c.CellReference = address).FirstOrDefault

        ' If the cell doesn't exist, create it:
        If theCell Is Nothing Then
            theCell = InsertCell(_workSheet, address)
        End If
        Return theCell
    End Function

    ''' <summary>
    ''' Gets the cell value.
    ''' </summary>
    ''' <param name="workSheetCell">The cell.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCellValue(ByVal workSheetCell As Cell) As String

        Dim value As String = ""
        ' If the cell doesn't exist, return an empty string.
        If workSheetCell IsNot Nothing Then
            value = workSheetCell.InnerText

            ' If the cell represents an integer number, you're done. 
            ' For dates, this code returns the serialized value that 
            ' represents the date. The code handles strings and booleans
            ' individually. For shared strings, the code looks up the corresponding
            ' value in the shared string table. For booleans, the code converts 
            ' the value into the words TRUE or FALSE.
            If workSheetCell.DataType IsNot Nothing Then
                Select Case workSheetCell.DataType.Value
                    Case CellValues.SharedString
                        ' For shared strings, look up the value in the shared strings table.
                        Dim stringTable = _workBookPart.GetPartsOfType(Of SharedStringTablePart).FirstOrDefault()
                        ' If the shared string table is missing, something's wrong.
                        ' Just return the index that you found in the cell.
                        ' Otherwise, look up the correct text in the table.
                        If stringTable IsNot Nothing Then
                            value = stringTable.SharedStringTable.ElementAt(Integer.Parse(value)).InnerText
                        End If
                    Case CellValues.Boolean
                        Select Case value
                            Case "0"
                                value = "FALSE"
                            Case Else
                                value = "TRUE"
                        End Select
                End Select
            End If
        End If
        Return value

    End Function

#End Region

#Region " COLUMN - SHARED "

    ''' <summary>
    ''' Gets the name of the column.
    ''' Given a cell name, parses the specified cell to get the column name.
    ''' </summary>
    ''' <param name="cellName">Name of the cell.</param><returns></returns>
    Public Shared Function GetColumnName(ByVal cellName As String) As String
        ' Create a regular expression to match the column name portion of the cell name.
        Dim regex As Regex = New Regex("[A-Za-z]+")
        Dim match As Match = regex.Match(cellName)
        Return match.Value
    End Function

    ''' <summary>
    ''' Gets the name of the column.
    ''' </summary>
    ''' <param name="column">The column.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetColumnName(ByVal column As Integer) As String

        ' This algorithm was found here:
        ' http://stackoverflow.com/questions/181596/how-to-convert-a-column-number-eg-127-into-an-excel-column-eg-aa

        ' Given a column number, retrieve the corresponding
        ' column string name:
        Dim value As Integer
        Dim remainder As Integer
        Dim result As String = String.Empty
        value = column

        Do While (value > 0)
            remainder = (value - 1) Mod 26
            result = Chr(65 + remainder) & result
            value = CInt(Int((value - remainder) / 26))
        Loop
        Return result
    End Function

    ''' <summary>
    ''' Gets the Excel sheet column number from the column name.
    ''' </summary>
    ''' <param name="colAdress">The col adress.</param><returns></returns>
    Public Shared Function GetColumnNumber(ByVal colAdress As String) As Integer

        Dim digits As Integer() = New Integer(colAdress.Length - 1) {}
        For i As Integer = 0 To colAdress.Length - 1
            digits(i) = Convert.ToInt32(colAdress(i)) - 64
        Next
        Dim mul As Integer = 1
        Dim res As Integer = 0
        For pos As Integer = digits.Length - 1 To 0 Step -1
            res += digits(pos) * mul
            mul *= 26
        Next
        Return res

    End Function

#End Region

#Region " DOCUMENT, WORKBOOK, WORKSHEET "

    Private _document As SpreadsheetDocument
    Private _workBookPart As WorkbookPart
    Private _sheet As Sheet
    Private _workSheetPart As WorksheetPart
    Private _workSheet As Worksheet

    ''' <summary>
    ''' Gets the name of the work sheet.
    ''' </summary>
    ''' <value>
    ''' The name of the work sheet.
    ''' </value>
    Public ReadOnly Property WorkSheetName As String
        Get
            If _workSheet IsNot Nothing Then
                Return _workSheet.LocalName
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' Opens the work sheet.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <param name="sheetName">Name of the sheet.</param><returns></returns>
    Public Function OpenWorkSheet(ByVal fileName As String, ByVal sheetName As String) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        ' Open the document for editing.
        _document = SpreadsheetDocument.Open(fileName, True)
        Dim _workBookPart As WorkbookPart = _document.WorkbookPart

        _sheet = _workBookPart.Workbook.Descendants(Of Sheet)().Where(Function(s) s.Name = sheetName).FirstOrDefault()

        If _sheet IsNot Nothing Then
            _workSheetPart = CType(_workBookPart.GetPartById(_sheet.Id), WorksheetPart)
            _workSheet = _workSheetPart.Worksheet
            returnValue = True
        Else
            returnValue = True
        End If

        Return returnValue
    End Function

    ''' <summary>
    ''' Saves the work book object model into the work book part.
    ''' This does not save into the file. 
    ''' </summary><returns></returns>
    Public Function SaveWorkBookModel() As Boolean

        Dim returnValue As Boolean = True

        If _workBookPart IsNot Nothing AndAlso _workBookPart.Workbook IsNot Nothing Then
            ' Saves the work book.
            _workBookPart.Workbook.Save()
        End If
        Return returnValue

    End Function

    ''' <summary>
    ''' Save and closes the document.
    ''' </summary><returns></returns>
    Public Function CloseDocument() As Boolean

        Dim returnValue As Boolean = True

        ' close the document for editing.
        If _document IsNot Nothing Then
            If _document.WorkbookPart IsNot Nothing AndAlso _document.WorkbookPart.Workbook IsNot Nothing Then
                If _document.WorkbookPart.Workbook.CalculationProperties IsNot Nothing Then
                    _document.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = True
                End If
                _document.WorkbookPart.Workbook.Save()
            End If
            _document.Close()
        End If

        Return returnValue
    End Function

#End Region

#Region " IMAGE "

    ''' <summary>
    ''' Inserts the image.
    ''' </summary>
    ''' <param name="left">The left position in English Metric Units.</param>
    ''' <param name="top">The top.</param>
    ''' <param name="fileName">Name of the file.</param><returns></returns>
    Public Function InsertImage(ByVal left As Long, ByVal top As Long, ByVal fileName As String) As Boolean
        Return Me._workSheet IsNot Nothing AndAlso Excel.InsertImage(Me._workSheet, left, top, fileName)
    End Function

    ''' <summary>
    ''' Inserts the image.
    ''' </summary>
    ''' <param name="left">The left position in Inches.</param>
    ''' <param name="top">The top position in Inches.</param>
    ''' <param name="width">The width in Inches.</param>
    ''' <param name="height">The height in Inches.</param>
    ''' <param name="fileName">Name of the file.</param><returns></returns>
    Public Function InsertImageFromInches(ByVal left As Single, ByVal top As Single, ByVal width As Single, ByVal height As Single, ByVal fileName As String) As Boolean
        Return Me._workSheet IsNot Nothing AndAlso Excel.InsertImageFromInches(Me._workSheet, left, top, width, height, fileName)
    End Function

    ''' <summary>
    ''' Inserts the image.
    ''' </summary>
    ''' <param name="left">The left position in Millimeters.</param>
    ''' <param name="top">The top position in Millimeters.</param>
    ''' <param name="width">The width in Millimeters.</param>
    ''' <param name="height">The height in Millimeters.</param>
    ''' <param name="fileName">Name of the file.</param><returns></returns>
    Public Function InsertImageFromMillimeters(ByVal left As Single, ByVal top As Single, ByVal width As Single, ByVal height As Single, ByVal fileName As String) As Boolean
        Return Me._workSheet IsNot Nothing AndAlso Excel.InsertImageFromMillimeters(Me._workSheet, left, top, width, height, fileName)
    End Function

    ''' <summary>
    ''' Reorders the image parts.
    ''' </summary>
    ''' <param name="worksheetPart">The Woirksheet Part.</param>
    ''' <param name="drawingsPart">The Drawings Part.</param>
    ''' <remarks>
    ''' This code snippet is intended to address inserting an image when the header has an image.
    ''' Source: http://polymathprogrammer.com/store/spreadsheet-open-xml/
    ''' Vincent Tan noticed that:
    ''' "...if you look at sheet2.xml of the unreadable worksheet (rename as .zip, unzip, then search under /xl/worksheets/), 
    ''' you’ll find at the bottom there are 2 tags, “drawing” (for “normal” images) and “legacyDrawingHF” 
    ''' (the header image is referenced here). The “drawing” tag needs to be before “legacyDrawingHF”.
    ''' When you create a blank workbook, then add in the header image, the “legacyDrawingHF” tag is written (by Excel). 
    ''' When you then use the code to add in images, the “drawing” tag is incorrectly placed after the “legacyDrawingHF”. 
    ''' The code doesn’t cover tag order placement. Although in the guide, I’ve advised customers to take note of the tag order 
    ''' (but of course, *no one* really reads the guide… :). I’ve had some customers say they’ve gotten corrupted files, and it’s 
    ''' usually because of the tag order. MergeCells is a particular offender… The Drawing tag is a particularly nasty one to deal
    ''' with, because it’s right down there in the order sequence within Worksheet. I’ve attached a snippet of code to help you 
    ''' with the correct position/appending. Incidentally, I’m working on a spreadsheet library, which is why I’ve been working 
    ''' hard at making this tag order thing/problem go away. The “dp” refers to the DrawingsPart class.
    ''' The snippet of code works for when you have a null DrawingsPart (the header image doesn’t count as an image for DrawingsPart,
    ''' because it’s a VML drawing). And the reason why the workaround you gave works is because you’ve already appended an image 
    ''' (thus a non-null DrawingsPart, thus a “drawing” tag into Worksheet). The multi-inserting code then grabs the existing Drawing 
    ''' tag (which is already in the correct tag order because Excel made it so) and work with that.
    ''' 
    ''' As for the code snippet, you could consolidate all the if statement conditions together if you like.
    ''' I just break them into 3’s so it’s easier to read. Basically, you search for all the tags/classes 
    ''' that could possibly be in front of the “drawing” tag, and then place “drawing” behind the last one.
    ''' </remarks>
    Private Shared Function InsertDrawing(ByVal worksheetPart As WorksheetPart, ByVal drawingsPart As DrawingsPart) As Drawing

        Dim drawing As New Drawing()
        drawing.Id = worksheetPart.GetIdOfPart(drawingsPart)

        Dim bFound As Boolean = False

        Dim oxe As OpenXmlElement = worksheetPart.Worksheet.FirstChild

        For Each child As OpenXmlElement In worksheetPart.Worksheet.ChildElements

            ' start with SheetData because it's a required child element
            If TypeOf child Is SheetData OrElse TypeOf child Is SheetCalculationProperties OrElse TypeOf child Is SheetProtection Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is ProtectedRanges OrElse TypeOf child Is Scenarios OrElse TypeOf child Is AutoFilter Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is SortState OrElse TypeOf child Is DataConsolidate OrElse TypeOf child Is CustomSheetViews Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is MergeCells OrElse TypeOf child Is PhoneticProperties OrElse TypeOf child Is ConditionalFormatting Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is DataValidations OrElse TypeOf child Is Hyperlinks OrElse TypeOf child Is PrintOptions Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is PageMargins OrElse TypeOf child Is PageSetup OrElse TypeOf child Is HeaderFooter Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is RowBreaks OrElse TypeOf child Is ColumnBreaks OrElse TypeOf child Is CustomProperties Then
                oxe = child
                bFound = True
            ElseIf TypeOf child Is CellWatches OrElse TypeOf child Is IgnoredErrors OrElse TypeOf child Is SmartTags Then
                oxe = child
                bFound = True
            End If
        Next

        If bFound Then
            Return worksheetPart.Worksheet.InsertAfter(drawing, oxe)
        Else
            Return worksheetPart.Worksheet.PrependChild(drawing)
        End If

    End Function

    ''' <summary>
    ''' Inserts the image.
    ''' </summary>
    ''' <param name="workSheet">The work sheet.</param>
    ''' <param name="left">The left position in English Metric Units.</param>
    ''' <param name="top">The top position in English Metric Units.</param>
    ''' <param name="width">The width in English Metric Units.</param>
    ''' <param name="height">The height in English Metric Units.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks>
    ''' Source: http://polymathprogrammer.com/store/spreadsheet-open-xml/
    ''' image position and size are in English Metric Units.
    ''' See http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
    ''' http://www.oreillynet.com/xml/blog/2007/04/what_is_an_emu.html
    ''' Image position conversion requires a fudge factor of 1.44 when converting from millimeters or inches.
    ''' Code fails if the sheet has a picture in the header. The work around is to add a picture to the body of the sheet.
    ''' </remarks>
    Public Shared Function InsertImage(ByVal workSheet As Worksheet,
                                       ByVal left As Long, ByVal top As Long,
                                       ByVal width As Nullable(Of Long), ByVal height As Nullable(Of Long),
                                       ByVal fileName As String) As Boolean
        Try
            Dim wsp As WorksheetPart = workSheet.WorksheetPart
            Dim dp As DrawingsPart
            Dim imgp As ImagePart
            Dim wsd As WorksheetDrawing

            Dim ipt As ImagePartType
            Select Case fileName.Substring(fileName.LastIndexOf("."c) + 1).ToLowerInvariant
                Case "bmp"
                    ipt = ImagePartType.Bmp
                Case "png"
                    ipt = ImagePartType.Png
                Case "jpg", "jpeg"
                    ipt = ImagePartType.Jpeg
                Case "gif"
                    ipt = ImagePartType.Gif
                Case Else
                    Return False
            End Select

            If wsp.DrawingsPart Is Nothing Then
                '----- no drawing part exists, add a new one
                dp = wsp.AddNewPart(Of DrawingsPart)()
                imgp = dp.AddImagePart(ipt)
                wsd = New WorksheetDrawing()
            Else
                '----- use existing drawing part
                dp = wsp.DrawingsPart
                imgp = dp.AddImagePart(ipt)
                wsd = dp.WorksheetDrawing
            End If

            Using fs As System.IO.FileStream = New System.IO.FileStream(fileName, System.IO.FileMode.Open)
                imgp.FeedData(fs)
            End Using

            Dim imageNumber As Integer = dp.ImageParts.Count() '  dp.ImageParts.Count(Of ImagePart)()
            If imageNumber = 1 Then
                InsertDrawing(wsp, dp)
            End If

            Dim nvdp As NonVisualDrawingProperties = New NonVisualDrawingProperties()
            nvdp.Id = New UInt32Value(CUInt(1024 + imageNumber))
            nvdp.Name = "Picture " & imageNumber.ToString()
            nvdp.Description = ""
            Dim picLocks As DocumentFormat.OpenXml.Drawing.PictureLocks = New DocumentFormat.OpenXml.Drawing.PictureLocks()
            picLocks.NoChangeAspect = True
            picLocks.NoChangeArrowheads = True
            Dim nvpdp As NonVisualPictureDrawingProperties = New NonVisualPictureDrawingProperties()
            nvpdp.PictureLocks = picLocks
            Dim nvpp As NonVisualPictureProperties = New NonVisualPictureProperties()
            nvpp.NonVisualDrawingProperties = nvdp
            nvpp.NonVisualPictureDrawingProperties = nvpdp

            Dim stretch As DocumentFormat.OpenXml.Drawing.Stretch = New DocumentFormat.OpenXml.Drawing.Stretch()
            stretch.FillRectangle = New DocumentFormat.OpenXml.Drawing.FillRectangle()

            Dim blipFill As BlipFill = New BlipFill()
            Dim blip As DocumentFormat.OpenXml.Drawing.Blip = New DocumentFormat.OpenXml.Drawing.Blip()
            blip.Embed = dp.GetIdOfPart(imgp)
            blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
            blipFill.Blip = blip
            blipFill.SourceRectangle = New DocumentFormat.OpenXml.Drawing.SourceRectangle()
            blipFill.Append(stretch)

            Dim t2d As DocumentFormat.OpenXml.Drawing.Transform2D = New DocumentFormat.OpenXml.Drawing.Transform2D()
            Dim offset As DocumentFormat.OpenXml.Drawing.Offset = New DocumentFormat.OpenXml.Drawing.Offset()
            offset.X = 0
            offset.Y = 0
            t2d.Offset = offset

            Dim extents As DocumentFormat.OpenXml.Drawing.Extents = New DocumentFormat.OpenXml.Drawing.Extents()
            Using bm As Bitmap = New Bitmap(fileName)

                If Not width.HasValue Then
                    extents.Cx = CLng(Fix(bm.Width)) * CLng(Fix(CSng(914400) / bm.HorizontalResolution))
                Else
                    extents.Cx = CType(width, Int64Value)
                End If

                If Not height.HasValue Then
                    extents.Cy = CLng(Fix(bm.Height)) * CLng(Fix(CSng(914400) / bm.VerticalResolution))
                Else
                    extents.Cy = CType(height, Int64Value)
                End If
            End Using

            t2d.Extents = extents
            Dim sp As ShapeProperties = New ShapeProperties()
            sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto
            sp.Transform2D = t2d
            Dim prstGeom As DocumentFormat.OpenXml.Drawing.PresetGeometry = New DocumentFormat.OpenXml.Drawing.PresetGeometry()
            prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle
            prstGeom.AdjustValueList = New DocumentFormat.OpenXml.Drawing.AdjustValueList()
            sp.Append(prstGeom)
            sp.Append(New DocumentFormat.OpenXml.Drawing.NoFill())

            Dim picture As DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture = New DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture()
            picture.NonVisualPictureProperties = nvpp
            picture.BlipFill = blipFill
            picture.ShapeProperties = sp

            Dim pos As Position = New Position()
            pos.X = left
            pos.Y = top
            Dim ext As Extent = New Extent()
            ext.Cx = extents.Cx
            ext.Cy = extents.Cy
            Dim anchor As AbsoluteAnchor = New AbsoluteAnchor()
            anchor.Position = pos
            anchor.Extent = ext
            anchor.Append(picture)
            anchor.Append(New ClientData())
            wsd.Append(anchor)
            wsd.Save(dp)
            Return True
        Catch
            Throw
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Inserts the image.
    ''' </summary>
    ''' <param name="worksheet">The work sheet.</param>
    ''' <param name="left">The left position in English Metric Units.</param>
    ''' <param name="top">The top position in English Metric Units.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImage(ByVal worksheet As Worksheet, ByVal left As Long, ByVal top As Long, ByVal fileName As String) As Boolean
        Return InsertImage(worksheet, left, top, Nothing, Nothing, fileName)
    End Function

    Const scaleFactor As Single = 1.44
    ''' <summary>
    ''' Inserts the image using Inch Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Inches.</param>
    ''' <param name="top">The top position in Inches.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromInches(ByVal worksheet As Worksheet, ByVal left As Double, ByVal top As Double, ByVal width As Double, ByVal height As Double, ByVal fileName As String) As Boolean
        Dim ps As PageSetup = worksheet.GetFirstChild(Of PageSetup)()
        Dim scaleFactor As Double = 1
        If ps.Scale IsNot Nothing Then scaleFactor = 100 / CDbl(ps.Scale)
        Return InsertImage(worksheet, EnglishMetricUnits.ConvertFromInches(scaleFactor * left), EnglishMetricUnits.ConvertFromInches(scaleFactor * top),
                           EnglishMetricUnits.ConvertFromInches(scaleFactor * width), EnglishMetricUnits.ConvertFromInches(scaleFactor * height), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Inch Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Inches.</param>
    ''' <param name="top">The top position in Inches.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromInches(ByVal worksheet As Worksheet, ByVal left As Double, ByVal top As Double, ByVal fileName As String) As Boolean
        Dim ps As PageSetup = worksheet.GetFirstChild(Of PageSetup)()
        Dim scaleFactor As Double = 1
        If ps.Scale IsNot Nothing Then scaleFactor = 100 / CDbl(ps.Scale)
        Return InsertImage(worksheet, EnglishMetricUnits.ConvertFromInches(scaleFactor * left), EnglishMetricUnits.ConvertFromInches(scaleFactor * top), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Inch Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Inches.</param>
    ''' <param name="top">The top position in Inches.</param>
    ''' <param name="width">The width in Inches.</param>
    ''' <param name="height">The height in Inches.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromInches(ByVal worksheet As Worksheet, ByVal left As Single, ByVal top As Single, ByVal width As Single, ByVal height As Single, ByVal fileName As String) As Boolean
        Return InsertImageFromInches(worksheet, CDbl(left), CDbl(top), CDbl(width), CDbl(height), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Inch Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Inches.</param>
    ''' <param name="top">The top position in Inches.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromInches(ByVal worksheet As Worksheet, ByVal left As Single, ByVal top As Single, ByVal fileName As String) As Boolean
        Return InsertImageFromInches(worksheet, CDbl(left), CDbl(top), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Millimeter Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Millimeters.</param>
    ''' <param name="top">The top position in Millimeters.</param>
    ''' <param name="width">The width in Millimeters.</param>
    ''' <param name="height">The height in Millimeters.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromMillimeters(ByVal worksheet As Worksheet, ByVal left As Double, ByVal top As Double, ByVal width As Double, ByVal height As Double, ByVal fileName As String) As Boolean
        Dim ps As PageSetup = worksheet.GetFirstChild(Of PageSetup)()
        Dim scaleFactor As Double = 1
        If ps.Scale IsNot Nothing Then scaleFactor = 100 / CDbl(ps.Scale)
        Return InsertImage(worksheet, EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * left), EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * top),
                           EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * width), EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * height), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Millimeter Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Millimeters.</param>
    ''' <param name="top">The top position in Millimeters.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromMillimeters(ByVal worksheet As Worksheet, ByVal left As Double, ByVal top As Double, ByVal fileName As String) As Boolean
        Dim ps As PageSetup = worksheet.GetFirstChild(Of PageSetup)()
        Dim scaleFactor As Double = 1
        If ps.Scale IsNot Nothing Then scaleFactor = 100 / CDbl(ps.Scale)
        Return InsertImage(worksheet, EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * left), EnglishMetricUnits.ConvertFromMillimeters(scaleFactor * top), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Millimeter Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Millimeters.</param>
    ''' <param name="top">The top position in Millimeters.</param>
    ''' <param name="width">The width in Millimeters.</param>
    ''' <param name="height">The height in Millimeters.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromMillimeters(ByVal worksheet As Worksheet, ByVal left As Single, ByVal top As Single, ByVal width As Single, ByVal height As Single, ByVal fileName As String) As Boolean
        Return InsertImageFromMillimeters(worksheet, CDbl(left), CDbl(top), CDbl(width), CDbl(height), fileName)
    End Function

    ''' <summary>
    ''' Inserts the image using Millimeter Coordinates.
    ''' </summary>
    ''' <param name="worksheet">The worksheet.</param>
    ''' <param name="left">The left position in Millimeters.</param>
    ''' <param name="top">The top position in Millimeters.</param>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertImageFromMillimeters(ByVal worksheet As Worksheet, ByVal left As Single, ByVal top As Single, ByVal fileName As String) As Boolean
        Return InsertImageFromMillimeters(worksheet, CDbl(left), CDbl(top), fileName)
    End Function

#End Region

#Region " ROW SHARED "

    ''' <summary>
    ''' Hides a row.
    ''' </summary>
    ''' <param name="row">The row.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function HideShowRow(ByVal row As Row, ByVal hide As Boolean) As Boolean

        ' Assume failure.
        Dim returnValue As Boolean = False

        If row IsNot Nothing Then
            row.Hidden = hide
            returnValue = True
        End If
        Return returnValue

    End Function

    ''' <summary>
    ''' Gets the index of the row.
    ''' Given a cell name, parses the specified cell to get the row index.
    ''' </summary>
    ''' <param name="cellName">Name of the cell.</param><returns></returns>
    Public Shared Function GetRowIndex(ByVal cellName As String) As Integer
        ' Create a regular expression to match the row index portion the cell name.
        Dim regex As Regex = New Regex("\d+")
        Dim match As Match = regex.Match(cellName)
        Return Integer.Parse(match.Value)
    End Function

#End Region

#Region " ROW "

    ''' <summary>
    ''' Gets the row.
    ''' </summary>
    ''' <param name="cellname">Name of the col.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRow(ByVal cellname As String) As Row
        Dim rowIndex As UInteger = CUInt(GetRowIndex(cellname))
        Dim rows As IEnumerable(Of Row) = _workSheet.Descendants(Of Row)().Where(Function(r) r.RowIndex.Value = rowIndex)
        Return rows.FirstOrDefault
    End Function

#End Region

#Region " STYLE "

    ''' <summary>
    ''' Creates the fill.
    ''' </summary>
    ''' <param name="styleSheet">The style sheet.</param>
    ''' <param name="fillColor">Color of the fill.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createFill(styleSheet As Stylesheet, fillColor As System.Drawing.Color) As UInt32Value
        Dim fill As New Fill(New PatternFill(New ForegroundColor() With {.Rgb = New HexBinaryValue() With { _
          .Value = System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(fillColor.A, fillColor.R, fillColor.G, fillColor.B)).Replace("#", "") _
         } _
        }) With {.PatternType = PatternValues.Solid _
        })
        styleSheet.Fills.Append(fill)

        Dim result As UInt32Value = styleSheet.Fills.Count
        styleSheet.Fills.Count = CType(styleSheet.Fills.LongCount + 1, UInt32Value)
        Return result
    End Function

    Private Function createFill1(styleSheet As Stylesheet, fillColor As System.Drawing.Color) As UInt32Value
        Dim html As String = System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(fillColor.A, fillColor.R, fillColor.G, fillColor.B)).Replace("#", "")
        Dim fill As New Fill()
        fill.PatternFill.ForegroundColor.Rgb = New HexBinaryValue(html)
        fill.PatternFill.PatternType = PatternValues.Solid
        styleSheet.Fills.Append(fill)
        Dim result As UInt32Value = styleSheet.Fills.Count
        styleSheet.Fills.Count = CType(styleSheet.Fills.LongCount + 1, UInt32Value)
        Return result
    End Function

    Private Function createFont(styleSheet As Stylesheet, fontName As String, fontSize As Nullable(Of Double), isBold As Boolean, foreColor As System.Drawing.Color) As UInt32Value

        Dim font As New DocumentFormat.OpenXml.Spreadsheet.Font()
        If Not String.IsNullOrEmpty(fontName) Then
            Dim name As New FontName() With {.Val = fontName}
            font.Append(name)
        End If

        If fontSize.HasValue Then
            Dim size As New FontSize() With {.Val = fontSize.Value}
            font.Append(size)
        End If

        If isBold = True Then
            Dim bold As New Bold()
            font.Append(bold)
        End If

        Dim color As New DocumentFormat.OpenXml.Spreadsheet.Color() With { _
            .Rgb = New HexBinaryValue() With { _
            .Value = System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(foreColor.A, foreColor.R, foreColor.G, foreColor.B)).Replace("#", "") _
            } _
        }
        font.Append(color)

        styleSheet.Fonts.Append(font)
        Dim result As UInt32Value = styleSheet.Fonts.Count
        styleSheet.Fonts.Count = CType(styleSheet.Fonts.LongCount + 1, UInt32Value)
        Return result
    End Function

    ''' <summary>
    ''' Creates the cell format.
    ''' </summary>
    ''' <param name="styleSheet">The style sheet.</param>
    ''' <param name="fontIndex">Index of the font.</param>
    ''' <param name="fillIndex">Index of the fill.</param>
    ''' <param name="numberFormatId">The number format id.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createCellFormat(styleSheet As Stylesheet, fontIndex As UInt32Value, fillIndex As UInt32Value, numberFormatId As UInt32Value) As UInt32Value

        Dim cellFormat As New CellFormat()

        If fontIndex IsNot Nothing Then
            cellFormat.FontId = fontIndex
        End If

        If fillIndex IsNot Nothing Then
            cellFormat.FillId = fillIndex
        End If

        If numberFormatId IsNot Nothing Then
            cellFormat.NumberFormatId = numberFormatId
            cellFormat.ApplyNumberFormat = BooleanValue.FromBoolean(True)
        End If

        styleSheet.CellFormats.Append(cellFormat)

        Dim result As UInt32Value = styleSheet.CellFormats.Count
        styleSheet.CellFormats.Count = CType(styleSheet.CellFormats.LongCount + 1, UInt32Value)
        styleSheet.Save()
        Return result

    End Function

    ''' <summary>
    ''' Gets the cell value.
    ''' </summary>
    ''' <param name="workSheetCell">The cell.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SetCellBackground(ByVal workSheetCell As Cell, ByVal color As System.Drawing.Color) As Boolean

        If workSheetCell IsNot Nothing Then
            Dim _workStylePart As WorkbookStylesPart = _document.WorkbookPart.WorkbookStylesPart
            Dim _workStyleSheet As Stylesheet = _document.WorkbookPart.WorkbookStylesPart.Stylesheet
            Dim fillIndex As UInt32Value = createFill(_workStyleSheet, color)
            Dim fontindex As UInt32Value = createFont(_workStyleSheet, "ArialNarrow", 8, False, System.Drawing.Color.Black)
            Dim styleIndex As UInt32Value = createCellFormat(_workStyleSheet, fontindex, fillIndex, Nothing)
            workSheetCell.StyleIndex = styleIndex
        End If
        Return True

    End Function

#End Region

End Class
